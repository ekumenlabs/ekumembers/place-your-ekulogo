#! /bin/env python3

# To overlay two images in python

import argparse
from PIL import Image
import numpy as np
from os import path

BACKGROUND_IMAGE_PATH = ("background.jpeg")
LOGO_PATH = ("logo.png")
BACKGROUND_SIZE = 244  # px
LOGO_SIZE = 50  # px
LOGO_PADDING = 15  # px


def superimpose_two_images(background_img_path, logo_path, bg_size, lg_size, padding):
    above_img = Image.open(logo_path)
    background = Image.open(background_img_path)

    # resize the background image without distorting the aspect rate
    size = None
    (h, w) = background.size

    if h > w:
        img_rate = w / h
        size = (bg_size, int(bg_size * img_rate))
    elif w > h:
        img_rate = h / w
        size = (int(bg_size * img_rate), bg_size)
    size = (bg_size,) * 2 if size is None else size
    background = background.resize(size, Image.ANTIALIAS)

    # resize the logo image
    logo_size = (lg_size,) * 2
    above_img = above_img.resize(logo_size, Image.ANTIALIAS)

    # paste to overlay two images
    logo_pos = (bg_size - lg_size - padding,) * 2
    background.paste(above_img, logo_pos, above_img)
    background.save('superimpose_two_images.png', "PNG")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Takes a profile picture and a logo, and superpose them'
    )
    parser.add_argument('--bg', dest='background_image',
                        default=path.join(path.dirname(
                            path.abspath(__file__)), BACKGROUND_IMAGE_PATH),
                        type=str, help='Sets the background image path')
    parser.add_argument('--logo', dest='logo_image',
                        default=path.join(path.dirname(
                            path.abspath(__file__)), LOGO_PATH),
                        type=str, help='Sets the logo image path')
    parser.add_argument('--bg_size', dest='background_size',
                        default=BACKGROUND_SIZE, type=int,
                        help='Sets the background image size')
    parser.add_argument('--logo_size', dest='logo_size',
                        default=LOGO_SIZE, type=int,
                        help='Sets the logo image size')
    parser.add_argument('--padding', dest='logo_padding',
                        default=LOGO_PADDING, type=int,
                        help='Sets the logo padding')
    args = parser.parse_args()

    superimpose_two_images(args.background_image, args.logo_image,
                           args.background_size, args.logo_size, args.logo_padding)
